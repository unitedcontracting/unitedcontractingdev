<?php

/**
 * auth
 */
Auth::routes(['verify' => true]);
Route::get('login/facebook', 'Auth\LoginController@redirectToProviderFacebook');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallbackFacebook');
Route::get('login/google', 'Auth\LoginController@redirectToProviderGoogle');
Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallbackGoogle');


Route::middleware(['verified'])->group(function () {

    /**
     * home routes
     */
    Route::get('/', 'HomeController@index')->name('homePage');
    Route::get('search', 'HomeController@search');

    /**
     * user routes
     */
    Route::get('profile-edit', 'User\UserController@edit');
    Route::patch('profile-update/{id}', 'User\UserController@update');
    Route::get('orders-user', 'User\UserController@showUserOrders');

    /**
     * about us
     */
    Route::get('about', 'AboutController@index');

    /**
     * shop
     */
    Route::get('shop', 'User\ShopController@index');

    /**
     * category
     */
    Route::get('category/{id}', 'User\CategoryController@indexCategory');

    /**
     * product
     */
    Route::get('sub_category/{id}', 'User\ProductController@index');
    Route::get('product_filter', 'User\ProductController@getFilters');

    /**
     * designer
     */
    Route::get('designer', 'User\DesignerController@index');
});