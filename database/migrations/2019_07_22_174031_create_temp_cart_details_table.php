<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempCartDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_cart_details', function (Blueprint $table) {
            $table->bigIncrements('IDTempCartDetails');
            $table->unsignedBigInteger('IDProduct');
            $table->unsignedBigInteger('IDTempCart');
            $table->timestamps();
            $table->foreign('IDProduct')->references('IDProduct')->on('products');
            $table->foreign('IDTempCart')->references('IDTempCart')->on('temp_cart');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_cart_details');
    }
}
