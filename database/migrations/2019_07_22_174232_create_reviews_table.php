<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->bigIncrements('IDReview');
            $table->unsignedBigInteger('ReviewedBy');
            $table->integer('ReviewedOn');
            $table->string('Body');
            $table->string('Type');
            $table->integer('Rating');
            $table->integer('Helpful');
            $table->tinyInteger('IsDeleted')->default(0);
            $table->timestamps();
            $table->foreign('ReviewedBy')->references('IDUser')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
