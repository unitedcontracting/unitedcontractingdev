<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsGalleryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_gallery', function (Blueprint $table) {
            $table->bigIncrements('IDProductGallery');
            $table->unsignedBigInteger('IDProduct');
            $table->string('ImageUrl');
            $table->tinyInteger('Main');
            $table->tinyInteger('IsDeleted')->default(0);
            $table->timestamps();
            $table->foreign('IDProduct')->references('IDProduct')->on('products');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_gallery');
    }
}
