<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('IDUser');
            $table->string('UserName');
            $table->string('Image')->nullable();
            $table->string('FirstName')->nullable();
            $table->string('LastName')->nullable();
            $table->string('Email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->integer('PhoneNumber')->nullable();
            $table->string('Password')->nullable();
            $table->unsignedBigInteger('IDRole')->nullable();
            $table->string('City')->nullable();
            $table->tinyInteger('Gender')->nullable();
            $table->string('Country')->nullable();
            $table->string('WebSite')->nullable();
            $table->date('BirthDate')->nullable();
            $table->integer('BlockedBy')->nullable();
            $table->tinyInteger('Paned')->nullable();
            $table->string('AboutMe')->nullable();
            $table->string('MyFavoriteStyle')->nullable();
            $table->string('MyNextHouseProject')->nullable();
            $table->tinyInteger('IsDeleted')->default(0);
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('IDRole')->references('IDRole')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
