<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->bigIncrements('IDAdmin');
            $table->string('UserName');
            $table->string('FirstName');
            $table->string('LastName');
            $table->string('Email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->integer('PhoneNumber');
            $table->string('Password');
            $table->unsignedBigInteger('IDAdminRole');
            $table->string('City');
            $table->tinyInteger('Gender');
            $table->string('Country');
            $table->date('BirthDate');
            $table->rememberToken();
            $table->tinyInteger('IsDeleted')->default(0);
            $table->timestamps();
            $table->foreign('IDAdminRole')->references('IDAdminRole')->on('admins_roles');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
