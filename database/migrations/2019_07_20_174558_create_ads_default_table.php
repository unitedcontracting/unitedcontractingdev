<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsDefaultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads_default', function (Blueprint $table) {
            $table->bigIncrements('IDAdDefault');
            $table->string('TypeOfAd');
            $table->string('Dimension')->nullable();
            $table->string('Position')->nullable();
            $table->unsignedBigInteger('IDSubscription');
            $table->tinyInteger('IsDeleted')->default(0);
            $table->timestamps();
            $table->foreign('IDSubscription')->references('IDSubscription')->on('subscriptions');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads_default');
    }
}
