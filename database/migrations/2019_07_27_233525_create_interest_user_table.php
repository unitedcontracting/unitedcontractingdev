<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterestUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interest_user', function (Blueprint $table) {
            $table->bigIncrements('IDInterestUser');
            $table->unsignedBigInteger('IDInterest');
            $table->unsignedBigInteger('IDUser');
            $table->timestamps();
            $table->foreign('IDInterest')->references('IDInterest')->on('interests');
            $table->foreign('IDUser')->references('IDUser')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interest_user');
    }
}
