<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complains', function (Blueprint $table) {
            $table->bigIncrements('IDComplain');
            $table->unsignedBigInteger('FromUser');
            $table->unsignedBigInteger('ToUser');
            $table->string('Body');
            $table->string('Title');
            $table->tinyInteger('IsDeleted')->default(0);
            $table->timestamps();
            $table->foreign('FromUser')->references('IDUser')->on('users');
            $table->foreign('ToUser')->references('IDUser')->on('users');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complains');
    }
}
