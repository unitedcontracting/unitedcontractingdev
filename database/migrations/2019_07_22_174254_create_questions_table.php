<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->bigIncrements('IDQuestion');
            $table->unsignedBigInteger('AskedBy');
            $table->integer('AskedOn');
            $table->string('Body');
            $table->string('Type');
            $table->tinyInteger('IsDeleted')->default(0);
            $table->timestamps();
            $table->foreign('AskedBy')->references('IDUser')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
