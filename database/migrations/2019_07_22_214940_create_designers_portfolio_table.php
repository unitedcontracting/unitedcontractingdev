<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignersPortfolioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designers_portfolio', function (Blueprint $table) {
            $table->bigIncrements('IDDesignerPortfolio');
            $table->string('Name');
            $table->string('Description')->nullable();
            $table->string('Specification')->nullable();
            $table->unsignedBigInteger('IDPortfolioCategory')->nullable();
            $table->unsignedBigInteger('IDUser');
            $table->float('Price')->nullable();
            $table->date('ProjectYear')->nullable();
            $table->string('ImagesAdsUrl')->nullable();
            $table->tinyInteger('IsDeleted')->default(0);
            $table->timestamps();
            $table->foreign('IDPortfolioCategory')->references('IDPortfolioCategory')->on('portfolio_categories');
            $table->foreign('IDUser')->references('IDUser')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designers_portfolio');
    }
}
