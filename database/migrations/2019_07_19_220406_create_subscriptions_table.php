<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->bigIncrements('IDSubscription');
            $table->unsignedBigInteger('IDUser');
            $table->string('Name');
            $table->string('Level')->nullable();
            $table->string('Type')->nullable();
            $table->string('SubscriptionPeriod')->nullable();
            $table->string('SponsoredAds')->nullable();
            $table->integer('NoOfAds');
            $table->integer('NoOfProducts');
            $table->integer('NoOf3DDraw');
            $table->integer('NoOfSponsoredAds');
            $table->tinyInteger('IsDeleted')->default(0);
            $table->timestamps();
            $table->foreign('IDUser')->references('IDUser')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
