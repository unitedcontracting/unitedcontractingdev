<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('IDComment');
            $table->unsignedBigInteger('CommentedBy');
            $table->integer('CommentedOn');
            $table->string('Body');
            $table->string('Type');
            $table->tinyInteger('Like');
            $table->tinyInteger('IsDeleted')->default(0);
            $table->timestamps();
            $table->foreign('CommentedBy')->references('IDUser')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
