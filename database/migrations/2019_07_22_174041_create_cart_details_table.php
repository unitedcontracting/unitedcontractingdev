<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_details', function (Blueprint $table) {
            $table->bigIncrements('IDCartDetails');
            $table->unsignedBigInteger('IDProduct');
            $table->unsignedBigInteger('IDCart');
            $table->timestamps();
            $table->foreign('IDProduct')->references('IDProduct')->on('products');
            $table->foreign('IDCart')->references('IDCart')->on('cart');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_details');
    }
}
