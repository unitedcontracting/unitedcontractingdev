<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_cart', function (Blueprint $table) {
            $table->bigIncrements('IDTempCart');
            $table->string('IDSession');
            $table->string('FinalImageUrl');
            $table->unsignedBigInteger('IDPromoCode')->nullable();
            $table->timestamps();
            $table->foreign('IDPromoCode')->references('IDPromoCode')->on('promo_codes');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_cart');
    }
}
