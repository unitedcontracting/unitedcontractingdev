<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsFavoriteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_favorite', function (Blueprint $table) {
            $table->bigIncrements('IDProductFavorite');
            $table->unsignedBigInteger('IDProduct');
            $table->unsignedBigInteger('IDUser');
            $table->timestamps();
            $table->foreign('IDProduct')->references('IDProduct')->on('products');
            $table->foreign('IDUser')->references('IDUser')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_favorite');
    }
}
