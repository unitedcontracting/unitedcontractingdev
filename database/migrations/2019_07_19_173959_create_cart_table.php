<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart', function (Blueprint $table) {
            $table->bigIncrements('IDCart');
            $table->unsignedBigInteger('IDUser');
            $table->string('FinalImageUrl');
            $table->unsignedBigInteger('IDPromoCode')->nullable();
            $table->timestamps();
            $table->foreign('IDUser')->references('IDUser')->on('users');
            $table->foreign('IDPromoCode')->references('IDPromoCode')->on('promo_codes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart');
    }
}
