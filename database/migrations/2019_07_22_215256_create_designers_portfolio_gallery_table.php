<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignersPortfolioGalleryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designers_portfolio_gallery', function (Blueprint $table) {
            $table->bigIncrements('IDDesignerPortfolioGallery');
            $table->string('ImageUrl');
            $table->tinyInteger('IsDeleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designers_portfolio_gallery');
    }
}
