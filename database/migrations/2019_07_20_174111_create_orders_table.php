<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('IDOrder');
            $table->unsignedBigInteger('IDUser');
            $table->unsignedBigInteger('IDCart');
            $table->float('Tax');
            $table->float('Shipping');
            $table->float('SubTotal');
            $table->float('Total');
            $table->tinyInteger('IsDeleted')->default(0);
            $table->timestamps();
            $table->foreign('IDUser')->references('IDUser')->on('users');
            $table->foreign('IDCart')->references('IDCart')->on('cart');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
