<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->unsignedBigInteger('IDStyle')->nullable();
            $table->unsignedBigInteger('IDBrand')->nullable();
            $table->unsignedBigInteger('IDColor')->nullable();
            $table->unsignedBigInteger('IDUsage')->nullable();
            $table->unsignedBigInteger('IDMaterial')->nullable();
            $table->unsignedBigInteger('IDShape')->nullable();
            $table->unsignedBigInteger('IDFinish')->nullable();
            $table->unsignedBigInteger('IDWidth')->nullable();
            $table->unsignedBigInteger('IDFeature')->nullable();
            $table->unsignedBigInteger('IDDesign')->nullable();
            $table->foreign('IDStyle')->references('IDDetails')->on('style');
            $table->foreign('IDBrand')->references('IDDetails')->on('brands');
            $table->foreign('IDColor')->references('IDDetails')->on('colors');
            $table->foreign('IDUsage')->references('IDDetails')->on('usages');
            $table->foreign('IDMaterial')->references('IDDetails')->on('materials');
            $table->foreign('IDShape')->references('IDDetails')->on('shapes');
            $table->foreign('IDFinish')->references('IDDetails')->on('finish');
            $table->foreign('IDWidth')->references('IDDetails')->on('widths');
            $table->foreign('IDFeature')->references('IDDetails')->on('features');
            $table->foreign('IDDesign')->references('IDDetails')->on('designs');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
