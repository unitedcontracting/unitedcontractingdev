<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_comments', function (Blueprint $table) {
            $table->bigIncrements('IDOrderComment');
            $table->unsignedBigInteger('IDSender');
            $table->unsignedBigInteger('IDReciver');
            $table->unsignedBigInteger('IDOrder');
            $table->string('Body');
            $table->timestamps();
            $table->foreign('IDSender')->references('IDUser')->on('users');
            $table->foreign('IDReciver')->references('IDUser')->on('users');
            $table->foreign('IDOrder')->references('IDOrder')->on('orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_comments');
    }
}
