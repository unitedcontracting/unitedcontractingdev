<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('IDProduct');
            $table->string('Name');
            $table->string('Description')->nullable();
            $table->string('Specification')->nullable();
            $table->unsignedBigInteger('IDCategory');
            $table->unsignedBigInteger('IDUser');
            $table->string('ShippingAndReturns')->nullable();
            $table->integer('StockQuantity')->nullable();
            $table->string('IDModel');
            $table->float('Price');
            $table->float('OnSalePrice');
            $table->string('ImagesAdsUrl');
            $table->string('Images3DDrawUrl')->nullable();
            $table->tinyInteger('IsDeleted')->default(0);
            $table->timestamps();
            $table->foreign('IDCategory')->references('IDCategory')->on('categories');
            $table->foreign('IDUser')->references('IDUser')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
