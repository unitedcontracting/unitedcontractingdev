<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('IDCategory');
            $table->string('Name');
            $table->unsignedBigInteger('IDParent')->nullable();
            $table->string('ImageUrl');
            $table->tinyInteger('IsDeleted')->default(0);
            $table->timestamps();
            $table->foreign('IDParent')->references('IDCategory')->on('categories');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
