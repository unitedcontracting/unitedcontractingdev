<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->bigIncrements('IDAd');
            $table->unsignedBigInteger('IDUser');
            $table->string('ImageUrl');
            $table->tinyInteger('Expire');
            $table->integer('CountOfView');
            $table->unsignedBigInteger('IDAdDefault');
            $table->tinyInteger('IsDeleted')->default(0);
            $table->string('Active')->nullable();
            $table->timestamps();
            $table->foreign('IDUser')->references('IDUser')->on('users');
            $table->foreign('IDAdDefault')->references('IDAdDefault')->on('ads_default');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
