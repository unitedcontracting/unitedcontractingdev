<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_details', function (Blueprint $table) {
            $table->bigIncrements('IDOrderDetail');
            $table->unsignedBigInteger('IDOrder');
            $table->unsignedBigInteger('IDProduct');
            $table->integer('Quantity');
            $table->string('Name');
            $table->float('Price');
            $table->timestamps();
            $table->foreign('IDProduct')->references('IDProduct')->on('products');
            $table->foreign('IDOrder')->references('IDOrder')->on('orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_details');
    }
}
