<?php

use Illuminate\Database\Seeder;

class rolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'IDRole' => 1,
            'Name' => 'user',
        ]);
        DB::table('roles')->insert([
            'IDRole' => 2,
            'Name' => 'designer',
        ]);

        DB::table('roles')->insert([
            'IDRole' => 3,
            'Name' => 'supplier',
        ]);
    }
}
