<?php

use Illuminate\Database\Seeder;

class ProductFavoriteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products_favorite')->insert([
            'IDUser' => 1000,
            'IDProduct' => 50,
        ]);
    }
}
