<?php

use Illuminate\Database\Seeder;

class CartDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cart_details')->insert([
            'IDCart' => 10,
            'IDProduct' => 50,
        ]);
        DB::table('cart_details')->insert([
            'IDCart' => 10,
            'IDProduct' => 51,
        ]);
    }
}
