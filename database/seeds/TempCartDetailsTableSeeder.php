<?php

use Illuminate\Database\Seeder;

class TempCartDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('temp_cart_details')->insert([
            'IDTempCart' => 10,
            'IDProduct' => 50,
        ]);
        DB::table('temp_cart_details')->insert([
            'IDTempCart' => 10,
            'IDProduct' => 51,
        ]);
    }
}
