<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'IDCategory' => 10,
            'IDParent' => null,
            'Name' => 'Living',
            'ImageUrl' => '/images/product1.jpg',
        ]);
        DB::table('categories')->insert([
            'IDCategory' => 11,
            'IDParent' => null,
            'Name' => 'BedRoom',
            'ImageUrl' => '/images/product2.jpg',
        ]);

        DB::table('categories')->insert([
            'IDCategory' => 12,
            'IDParent' => null,
            'Name' => 'Living',
            'ImageUrl' => '/images/product3.jpg',
        ]);
        DB::table('categories')->insert([
            'IDCategory' => 13,
            'IDParent' => null,
            'Name' => 'BedRoom',
            'ImageUrl' => '/images/product4.jpg',
        ]);
        DB::table('categories')->insert([
            'IDCategory' => 14,
            'IDParent' => null,
            'Name' => 'Living',
            'ImageUrl' => '/images/product5.jpg',
        ]);

        DB::table('categories')->insert([
            'IDCategory' => 15,
            'IDParent' => null,
            'Name' => 'BedRoom',
            'ImageUrl' => '/images/product6.jpg',
        ]);

        DB::table('categories')->insert([
            'IDCategory' => 16,
            'IDParent' => 10,
            'Name' => 'classic',
            'ImageUrl' => '/images/product3.jpg',
        ]);
        DB::table('categories')->insert([
            'IDCategory' => 17,
            'IDParent' => 11,
            'Name' => 'modern',
            'ImageUrl' => '/images/product4.jpg',
        ]);
    }
}
