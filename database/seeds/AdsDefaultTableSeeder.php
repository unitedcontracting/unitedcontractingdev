<?php

use Illuminate\Database\Seeder;

class AdsDefaultTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ads_default')->insert([
            'IDAdDefault' => 100,
            'TypeOfAd' => 'home',
            'IDSubscription' => 10,
        ]);
    }
}
