<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(rolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(subscriptionsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(AdsDefaultTableSeeder::class);
        $this->call(AdsTableSeeder::class);
        $this->call(TempCartTableSeeder::class);
        $this->call(TempCartDetailsTableSeeder::class);
        $this->call(ProductFavoriteTableSeeder::class);
        $this->call(CartTableSeeder::class);
        $this->call(CartDetailsTableSeeder::class);
    }
}
