<?php

use Illuminate\Database\Seeder;

class subscriptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subscriptions')->insert([
            'IDSubscription' => 10,
            'Name' => 'gold',
            'IDUser' => 1001,
            'NoOfAds' =>10,
            'NoOfProducts' =>15,
            'NoOfSponsoredAds'=>10,
            'NoOf3DDraw'=>5,
        ]);
    }
}
