<?php

use Illuminate\Database\Seeder;

class AdsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ads')->insert([
            'IDAd' =>100,
            'IDUser' => 1000,
            'ImageUrl' => '/images/slide-img1.jpg',
            'Expire' => 0,
            'CountOfView' => 10,
            'IDAdDefault' => 100,
            'IsDeleted' => 0,
            'Active' =>'active',
        ]);

        DB::table('ads')->insert([
            'IDAd' =>101,
            'IDUser' => 1000,
            'ImageUrl' => '/images/slide-img2.jpg',
            'Expire' => 0,
            'CountOfView' => 10,
            'IDAdDefault' => 100,
            'IsDeleted' => 0,
        ]);
    }
}
