<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'IDCategory' => 17,
            'IDProduct' => 50,
            'Name' => 'Bed',
            'ImagesAdsUrl' => '/images/product1.jpg',
            'IDUser' => 1001,
            'Price'=>1000,
            'OnSalePrice'=>900,
            'IDModel'=>'#1245',
            'Description'=>'product nice',
            'SoldQuantity' => 5,
        ]);

        DB::table('products')->insert([
            'IDCategory' => 16,
            'IDProduct' => 51,
            'Name' => 'Chair',
            'ImagesAdsUrl' => '/images/product2.jpg',
            'IDUser' => 1001,
            'Price'=>1200,
            'OnSalePrice'=>800,
            'IDModel'=>'#1245',
            'Description'=>'product good',
            'SoldQuantity' => 4,
        ]);

        DB::table('products')->insert([
            'IDCategory' => 17,
            'Name' => 'Bed',
            'ImagesAdsUrl' => '/images/product3.jpg',
            'IDUser' => 1001,
            'Price'=>1000,
            'OnSalePrice'=>900,
            'IDModel'=>'#1245',
            'Description'=>'product nice',
            'SoldQuantity' => 3,
        ]);

        DB::table('products')->insert([
            'IDCategory' => 16,
            'Name' => 'Chair',
            'ImagesAdsUrl' => '/images/product4.jpg',
            'IDUser' => 1001,
            'Price'=>1200,
            'OnSalePrice'=>800,
            'IDModel'=>'#1245',
            'Description'=>'product good',
            'SoldQuantity' => 2,
        ]);
        DB::table('products')->insert([
            'IDCategory' => 17,
            'Name' => 'Bed',
            'ImagesAdsUrl' => '/images/product5.jpg',
            'IDUser' => 1001,
            'Price'=>1000,
            'OnSalePrice'=>900,
            'IDModel'=>'#1245',
            'Description'=>'product nice',
            'SoldQuantity' => 1,
        ]);

        DB::table('products')->insert([
            'IDCategory' => 16,
            'Name' => 'Chair',
            'ImagesAdsUrl' => '/images/product6.jpg',
            'IDUser' => 1001,
            'Price'=>1200,
            'OnSalePrice'=>800,
            'IDModel'=>'#1245',
            'Description'=>'product good',
            'SoldQuantity' => 7,
        ]);
        DB::table('products')->insert([
            'IDCategory' => 17,
            'Name' => 'Bed',
            'ImagesAdsUrl' => '/images/product1.jpg',
            'IDUser' => 1001,
            'Price'=>1000,
            'OnSalePrice'=>900,
            'IDModel'=>'#1245',
            'Description'=>'product nice',
            'SoldQuantity' => 0,
        ]);

        DB::table('products')->insert([
            'IDCategory' => 16,
            'Name' => 'Chair',
            'ImagesAdsUrl' => '/images/product2.jpg',
            'IDUser' => 1001,
            'Price'=>1200,
            'OnSalePrice'=>800,
            'IDModel'=>'#1245',
            'Description'=>'product good',
            'SoldQuantity' => 8,
        ]);
    }
}
