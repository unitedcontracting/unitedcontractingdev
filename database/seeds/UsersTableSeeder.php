<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'IDUser' => 1000,
            'UserName' => Str::random(10),
            'Email' => Str::random(10) . '@gmail.com',
            'Password' => bcrypt('12345678'),
            'IDRole' => 1,
        ]);

        DB::table('users')->insert([
            'UserName' => Str::random(10),
            'Email' => Str::random(10) . '@gmail.com',
            'Password' => bcrypt('12345678'),
            'IDRole' => 2,
            'Image' =>'/images/user1.jpg'
        ]);
        DB::table('users')->insert([
            'UserName' => Str::random(10),
            'Email' => Str::random(10) . '@gmail.com',
            'Password' => bcrypt('12345678'),
            'IDRole' => 2,
            'Image' =>'/images/user2.jpg'
        ]);
        DB::table('users')->insert([
            'UserName' => Str::random(10),
            'Email' => Str::random(10) . '@gmail.com',
            'Password' => bcrypt('12345678'),
            'IDRole' => 2,
            'Image' =>'/images/user3.jpg'
        ]);
        DB::table('users')->insert([
            'UserName' => Str::random(10),
            'Email' => Str::random(10) . '@gmail.com',
            'Password' => bcrypt('12345678'),
            'IDRole' => 2,
            'Image' =>'/images/user4.jpg'
        ]);
        DB::table('users')->insert([
            'UserName' => Str::random(10),
            'Email' => Str::random(10) . '@gmail.com',
            'Password' => bcrypt('12345678'),
            'IDRole' => 2,
            'Image' =>'/images/user5.jpg'
        ]);
        DB::table('users')->insert([
            'UserName' => Str::random(10),
            'Email' => Str::random(10) . '@gmail.com',
            'Password' => bcrypt('12345678'),
            'IDRole' => 2,
            'Image' =>'/images/user4.jpg'
        ]);

        DB::table('users')->insert([
            'UserName' => Str::random(10),
            'LastName' => Str::random(10),
            'FirstName' => Str::random(10),
            'Email' => Str::random(10) . '@gmail.com',
            'Password' => bcrypt('12345678'),
            'IDRole' => 3,
        ]);
    }
}
