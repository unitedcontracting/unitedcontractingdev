<?php

use Illuminate\Database\Seeder;

class CartTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cart')->insert([
            'IDCart' =>10,
            'IDUser' => 1000,
            'FinalImageUrl' =>  '/images/1.png',
        ]);
    }
}
