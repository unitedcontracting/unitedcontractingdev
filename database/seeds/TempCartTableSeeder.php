<?php

use Illuminate\Database\Seeder;

class TempCartTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('temp_cart')->insert([
            'IDTempCart' => 10,
            'IDSession' => 1000,
            'FinalImageUrl' =>  '/images/1.png',
        ]);
    }
}
