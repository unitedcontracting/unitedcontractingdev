<?php

namespace App\Http\Controllers\Auth;

use App\CustomUserAuthenticatable;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

//    public function login(Request $request)
//    {
//        $this->validateLogin($request);
//
//        // If the class is using the ThrottlesLogins trait, we can automatically throttle
//        // the login attempts for this application. We'll key this by the username and
//        // the IP address of the client making these requests into this application.
//        if (method_exists($this, 'hasTooManyLoginAttempts') &&
//            $this->hasTooManyLoginAttempts($request)) {
//            $this->fireLockoutEvent($request);
//
//            return $this->sendLockoutResponse($request);
//        }
//
//        $user = User::where('Email', $request->email)->first();
//
//        if (!$user->email_verified_at) {
//            return redirect()
//                ->route('login')
//                ->with([ 'error' => 'you must verify your email first before you can login' ]);
//        }
//
//        if ($this->attemptLogin($request)) {
//            return $this->sendLoginResponse($request);
//        }
//
//        // If the login attempt was unsuccessful we will increment the number of attempts
//        // to login and redirect the user back to the login form. Of course, when this
//        // user surpasses their maximum number of attempts they will get locked out.
//        $this->incrementLoginAttempts($request);
//
//        return $this->sendFailedLoginResponse($request);
//    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProviderFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallbackFacebook()
    {
        $socialMediaUser = Socialite::driver('facebook')->user();

        $customAuthUser = $this->checkUser($socialMediaUser, 'facebook');

        Auth::login($customAuthUser, true);

        return redirect()->route('homePage');
    }

    /**
     * Redirect the user to the Google authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProviderGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from Google.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallbackGoogle()
    {
        $socialMediaUser = Socialite::driver('google')->user();
        $customAuthUser = $this->checkUser($socialMediaUser, 'google');

        Auth::login($customAuthUser, true);

        return redirect()->route('homePage');
    }

    private function checkUser($user, $type)
    {
        $dbUser = User::where('Email', $user->email)->first();

        if ($dbUser) {
            $dbUser->fill([
                'Token' => $user->token,
                'UserName' => $user->name,
                'TokenType' => $type,
                'email_verified_at' => Date::now(),
            ]);
        } else {
            $dbUser = User::create([
                'Email' => $user->email,
                'Token' => $user->token,
                'UserName' => $user->name,
                'TokenType' => $type,
                'email_verified_at' => Date::now(),
            ]);
        }

        return new CustomUserAuthenticatable($dbUser);
    }
}
