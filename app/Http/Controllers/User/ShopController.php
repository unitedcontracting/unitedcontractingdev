<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Ad;
use App\Models\Category;
use App\Models\Product;

class ShopController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $IDCategory = Category::first()->toArray()['IDCategory'];

        return $this->getDataView($IDCategory);
    }

    /**
     * @param $IDCategory
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDataView($IDCategory)
    {
        $GetAdsVar = Ad::where('IsDeleted', 0)->where('Expire', 0)->get()->toArray();

        $GetCategoriesVar = Category::where('IDParent', null)->get()->toArray();

        $GetCategoryWithSubCategoryVar = Category::where('IDParent', $IDCategory)->get()->toArray();

        $GetProductOnSaleVar = Product::where('IDCategory', $IDCategory)
            ->where('OnSalePrice', '>', 0)->get()->toArray();

        $GetProductNewArrivalVar = Product::where('IDCategory', $IDCategory)->orderBy('created_at', 'desc')
            ->take(10)->get()->toArray();

        return view('user.shop', compact(
            'GetProductOnSaleVar',
            'GetProductNewArrivalVar',
            'GetAdsVar',
            'GetCategoriesVar',
            'GetProductsVar',
            'GetCategoryWithSubCategoryVar'
        ));
    }
}
