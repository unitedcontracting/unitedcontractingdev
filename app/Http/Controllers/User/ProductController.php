<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Ad;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index($IDSubCategory)
    {
        $GetAdsVar = Ad::where('IsDeleted', 0)->where('Expire', 0)->get()->toArray();
//        $GetProductsVar = Product::latest('IDProduct')->paginate(10);
        $GetProductsVar = Product::where('IDCategory', $IDSubCategory)->get()->toArray();
        return view('user.product', compact('GetAdsVar', 'GetProductsVar'));
    }



    public function getFilters(Request $request)
    {

        $style = $request->input('IDStyle');
        $brand = $request->input('IDBrand');
        $color = $request->input('IDColor');
        $usage = $request->input('IDUsage');
        $material = $request->input('IDMaterial');
        $shape = $request->input('IDShape');
        $finish = $request->input('IDFinish');
        $width = $request->input('IDWidth');
        $feature = $request->input('IDFeature');
        $design = $request->input('IDDesign');

        $query = new Product;

        if ($style)
            $query = $query->where('IDStyle', $style);

        if ($brand)
            $query = $query->where('IDBrand', $brand);

        if ($color)
            $query = $query->where('IDColor', $color);

        if ($usage)
            $query = $query->where('IDUsage', $usage);

        if ($material)
            $query = $query->where('IDMaterial', $material);

        if ($shape)
            $query = $query->where('IDShape', $shape);


        if ($width)
            $query = $query->where('IDWidth', $width);

        if ($feature)
            $query = $query->where('IDFeature', $feature);

        if ($design)
            $query = $query->where('IDDesign', $design);

        if ($finish)
            $query = $query->where('IDFinish', $finish);


        $GetProductsVar = $query->get();
        // return $products;

        $GetAdsVar = Ad::where('IsDeleted', 0)->where('Expire', 0)->get()->toArray();

        return view('user.product_filter', compact(
            'GetAdsVar',
            'GetProductsVar'
        ));
    }
}
