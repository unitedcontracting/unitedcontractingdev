<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Ad;
use App\Models\Category;
use App\Models\Product;

class CategoryController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function indexCategory($IDCategory)
    {

        return $this->getDataView($IDCategory);
    }

    /**
     * @param $IDCategory
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDataView($IDCategory)
    {
        $GetAdsVar = Ad::where('IsDeleted', 0)->where('Expire', 0)->get()->toArray();

        $GetCategoriesVar = Category::where('IDParent', null)->get()->toArray();

        $GetCategoryWithSubCategoryVar = Category::where('IDParent', $IDCategory)->get()->toArray();

        $GetProductNewArrivalVar = Product::where('IDCategory', $IDCategory)->orderBy('created_at', 'desc')
            ->take(10)->get()->toArray();

        $GetProductsVar = Product::where('IDCategory', $IDCategory)->get()->toArray();

        $GetProductsBestSellerVar = Product::where('IDCategory', $IDCategory)->get()->toArray();

        $GetProductsReviewsVar = $this->productReviews();

        return view('user.category', compact(
            'GetProductsReviewsVar',
            'GetProductsBestSellerVar',
            'GetProductNewArrivalVar',
            'GetAdsVar',
            'GetCategoriesVar',
            'GetProductsVar',
            'GetCategoryWithSubCategoryVar'
        ));
    }

    public function productReviews()
    {
        $GetProductWithReviewsVar = Product::with('reviews')->whereHas('reviews', function ($query) {
            $query->where('Rating', '>', 0);
        })->get()->toArray();

        foreach ($GetProductWithReviewsVar as $product => $reviews) {
            $sumOfRating = 0;
            foreach ($reviews['reviews'] as $review) {
                $sumOfRating += $review['Rating'];
                $array[$reviews['IDProduct']]['id'] = $reviews['IDProduct'];
                $array[$reviews['IDProduct']]['rating'] = $sumOfRating / (count($reviews['reviews']));
                $array[$reviews['IDProduct']]['image'] = $reviews['ImagesAdsUrl'];
            }
        }

        $sortedRating = usort($array, function ($a, $b) {
            $retval = $a['rating'] <= $b['rating'];
            return $retval;
        });

        $bestProductRating = array_slice($array, 0, 10);

        return $bestProductRating;
    }


}
