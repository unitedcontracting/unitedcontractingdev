<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\DesignerPortfolio;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class DesignerController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $GetDesignerWithReviewsVar = User::with('reviewOn')->whereHas('reviewOn', function ($query) {
            $query->where('Rating', '>', 0);
        })->get()->toArray();
        foreach ($GetDesignerWithReviewsVar as $user => $reviews) {
            $sumOfRating = 0;
            foreach ($reviews['review_on'] as $review) {
                $sumOfRating += $review['Rating'];
                $UserPortfolioMainImage = DesignerPortfolio::where('IDUser', $reviews['IDUser'])->first();
                $array[$reviews['IDUser']]['ID'] = $reviews['IDUser'];
                $array[$reviews['IDUser']]['Rating'] = ($sumOfRating / (count($reviews['review_on'])));
                $array[$reviews['IDUser']]['Reviewer'] = (count($reviews['review_on']));
                $array[$reviews['IDUser']]['About'] = $reviews['AboutMe'];
                $array[$reviews['IDUser']]['Name'] = $reviews['UserName'];
                $array[$reviews['IDUser']]['Image'] = $reviews['Image'];
                $array[$reviews['IDUser']]['ImagePortfolio'] = $UserPortfolioMainImage['ImagesAdsUrl'];
                $array[$reviews['IDUser']]['RatingPercent'] =  $array[$reviews['IDUser']]['Rating'] *20 ;
            }
        }

        $sortedRating = usort($array, function ($a, $b) {
            $retval = $a['Rating'] <= $b['Rating'];
            return $retval;
        });

        $bestUserRating = array_slice($array, 0, 21);

        $bestUserRating = $this->pagination($bestUserRating, $request);

        return view('user.designer', compact('bestUserRating'));
    }


    public function pagination($bestUserRating, $request)
    {
        // Get current page form url e.x. &page=1
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        // Create a new Laravel collection from the array data
        $itemCollection = collect($bestUserRating);

        // Define how many items we want to be visible in each page
        $perPage = 3;

        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();

        // Create our paginator and pass it to the view
        $paginatedItems = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);

        // set url path for generted links
        $paginatedItems->setPath($request->url());

        return $paginatedItems;
    }
}
