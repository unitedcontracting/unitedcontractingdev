<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\User;
use App\Models\Order;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * view edit page to update profile
     */
    public function edit($IDUser)
    {
        $GetUserVar = User::find($IDUser);
        return View('', compact('GetUserVar'));
    }

    /**
     * update user profile
     */
    public function update($IDUser , Request $request)
    {
        User::find($IDUser)->update($request->all());

        return redirect('')->with('success','Updated successfully');
    }


    /**
     * get user orders
     */
    public function showUserOrders($IDUser)
    {
        $GetUserOrdersVar = Order::where('IDUser',$IDUser)->get();

        return View('', compact('GetUserOrdersVar'));
    }
}
