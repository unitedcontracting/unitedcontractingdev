<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductFavorite;
use App\Models\TempCart;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware(['verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth()->id()) {
            $UserIDVar = Auth()->id();
        } else {
            $UserIDVar = session()->getId();
        }

        $GetAdsVar = Ad::where('IsDeleted', 0)->where('Expire', 0)->get()->toArray();

        $GetCategoriesVar = Category::where('IDParent', null)->get()->toArray();

        $GetDesignersVar = User::where('IDRole', 2)->get()->toArray();

        $GetProductsVar = Product::paginate(6);

        $GetFavoriteProductsVar = count(ProductFavorite::where('IDUser', $UserIDVar)->get()->toArray());

        $GetCartVar = count(Cart::where('IDUser', $UserIDVar)->get()->toArray());

        $GetTempCartVar = count(TempCart::where('IDSession', $UserIDVar)->get()->toArray());

        return view('home', compact(
            'GetAdsVar',
            'GetTempCartVar',
            'GetCartVar',
            'GetFavoriteProductsVar',
            'GetProductsVar',
            'GetDesignersVar',
            'GetCategoriesVar',
            'UserIDVar'
        ));
    }

    /**
     * search
     */
    public function search(Request $request)
    {
        $validatedData = $request->validate([
            'search' => 'required',
        ]);

        $DataSearchVar = $request->search;

        $GetSearchProductVar = Product::where('Name', 'LIKE', '%' . $DataSearchVar . '%')
            ->orWhere('Description', 'LIKE', '%' . $DataSearchVar . '%')
            ->get()->toArray();

        $GetSearchCategoryVar = Category::where('Name', 'LIKE', '%' . $DataSearchVar . '%')->get()->toArray();

        $GetSearchSupplierVar = User::where('UserName', 'LIKE', '%' . $DataSearchVar . '%')
            ->orWhere('FirstName', 'LIKE', '%' . $DataSearchVar . '%')
            ->orWhere('LastName', 'LIKE', '%' . $DataSearchVar . '%')
            ->get()->toArray();

        $SearchResultVar = [$GetSearchProductVar, $GetSearchCategoryVar, $GetSearchSupplierVar];
        dd($SearchResultVar);

        return view('home', compact('SearchResultVar'));
    }

}
