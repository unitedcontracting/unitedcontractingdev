<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InterestUser extends Model
{
    protected $fillable = [
        'IDInterest', 'IDUser',
    ];

    protected $table ='interest_user';
}
