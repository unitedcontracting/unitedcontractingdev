<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Name', 'Level', 'Type','SubscriptionPeriod','SponsoredAds',
        'NoOfAds','NoOfProducts','NoOf3DDraw','NoOfSponsoredAds','IsDeleted',
    ];

    protected $table ='subscriptions';

    public function users()
    {
        return $this->hasMany('App\User');
    }
}
