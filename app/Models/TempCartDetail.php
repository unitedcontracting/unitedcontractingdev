<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TempCartDetail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'IDProduct', 'IDCart',
    ];
    protected $table ='temp_cart_details';
}
