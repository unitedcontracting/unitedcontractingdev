<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdDefault extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'TypeOfAd', 'Dimension', 'Position','IDSubscription','IsDeleted',
    ];

    protected $table ='ads_default';

    public function ads()
    {
        return $this->hasMany('App\Models\Ad');
    }
}
