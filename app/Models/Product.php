<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Name', 'Description', 'Specification', 'IDCategory', 'IDUser', 'Price', 'ShippingAndReturns', 'IsDeleted',
        'StockQuantity', 'IDModel', 'OnSalePrice', 'ImagesAdsUrl', 'Images3DDrawUrl',
        'IDStyle', 'IDBrand', 'IDColor', 'IDUsage', 'IDMaterial', 'IDShape', 'IDFinish', 'IDWidth', 'IDFeature', 'IDDesign',
    ];
    protected $table = 'products';

    protected $primaryKey = 'IDProduct';

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function productGallery()
    {
        return $this->hasMany('App\Models\ProductGallery');
    }

    public function usersFavorite()
    {
        return $this->belongsToMany('App\User');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function cart()
    {
        return $this->belongsTo('App\Models\Cart');
    }

    public function tempCart()
    {
        return $this->belongsTo('App\Models\Cart');
    }


    public function questions()
    {
        return $this->morphMany('App\Models\Question', 'questionable');
    }

    public function reviews()
    {
        return $this->morphMany('App\Models\Review', 'reviewable', 'Type', 'ReviewedOn');
    }


}
