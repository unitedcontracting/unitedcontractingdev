<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TempCart extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'IDSession', 'FinalImageUrl', 'IDPromoCode',
    ];
    protected $table ='temp_cart';

    public function promoCode()
    {
        return $this->belongsTo('App\Models\PromoCode');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }
}
