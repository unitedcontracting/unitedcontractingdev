<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderComment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'IDSender', 'IDReciver', 'IDOrder','Body',
    ];
    protected $table ='orders_comments';

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }
}
