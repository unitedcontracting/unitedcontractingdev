<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'IDUser', 'FinalImageUrl', 'IDPromoCode',
    ];
    protected $table ='cart';

    public function user()
    {
        return $this->hasOne('App\User');
    }

    public function promoCode()
    {
        return $this->belongsTo('App\Models\PromoCode');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

}
