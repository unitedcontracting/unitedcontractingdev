<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Complain extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'FromUser', 'ToUser', 'Body','Title','IsDeleted',
    ];

    protected $table ='complains';

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
