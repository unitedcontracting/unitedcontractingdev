<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'IDUser', 'IDCart', 'Tax','Shipping','SubTotal','Total','IsDeleted',
    ];

    protected $table ='orders';
    protected $primaryKey = 'IDOrder';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\OrderComment');
    }

}
