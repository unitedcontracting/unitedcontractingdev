<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ReviewedBy', 'ReviewedOn', 'Body','Type','Rating','Helpful','IsDeleted',
    ];

    protected $table ='reviews';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function reviewable()
    {
        return $this->morphTo('reviewable', 'Type', 'ReviewedOn', 'IDProduct');
    }
}
