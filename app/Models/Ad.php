<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'IDUser', 'ImageUrl', 'Expire','CountOfView','IDAdDefault','IsDeleted',
    ];
    protected $table ='ads';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function adDefault()
    {
        return $this->belongsTo('App\Models\AdDefault');
    }
}
