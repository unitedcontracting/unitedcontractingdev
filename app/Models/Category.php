<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Name', 'IDParent', 'ImageUrl','IsDeleted',
    ];

    protected $table ='categories';
    protected $primaryKey = 'IDCategory';


    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function parentCategory(){
        return $this->hasMany('App\Models\Category');
    }
    public function childCategory(){
        return $this->belongsTo('App\Models\Category');
    }

}
