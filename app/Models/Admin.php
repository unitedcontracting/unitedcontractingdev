<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'UserName', 'FirstName', 'LastName','Email','PhoneNumber','Password','IDAdminRole',
        'City','Gender','Country','BirthDate','IsDeleted',
    ];

    protected $table ='admins';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'Password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function adminRole()
    {
        return $this->belongsTo('App\Models\AdminRole');
    }
}
