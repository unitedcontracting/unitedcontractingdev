<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductGallery extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'IDProduct', 'ImageUrl','IsDeleted','Main',
    ];

    protected $table ='products_gallery';

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
