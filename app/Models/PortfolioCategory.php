<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PortfolioCategory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Name', 'IDParent', 'ImageUrl','IsDeleted',
    ];

    protected $table ='portfolio_categories';

    public function designerPortfolio()
    {
        return $this->hasMany('App\Models\DesignerPortfolio');
    }

    public function parentPortfolioCategory(){
        return $this->hasMany('App\Models\PortfolioCategory');
    }
    public function childPortfolioCategory(){
        return $this->belongsTo('App\Models\PortfolioCategory');
    }
}
