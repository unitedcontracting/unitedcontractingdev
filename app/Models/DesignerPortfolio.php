<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DesignerPortfolio extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Name', 'Description', 'Specification','IDPortfolioCategory','IDUser','Price','ProjectYear','IsDeleted',
    ];

    protected $table ='designers_portfolio';

    public function portfolioCategory()
    {
        return $this->belongsTo('App\Models\PortfolioCategory');
    }

    public function portfolioGallery()
    {
        return $this->hasMany('App\Models\DesignerPortfolioGallery');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }


}
