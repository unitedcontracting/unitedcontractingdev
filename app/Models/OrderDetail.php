<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'IDOrder', 'IDProduct', 'Quantity','Name','Price',
    ];

    protected $table ='orders_details';
}
