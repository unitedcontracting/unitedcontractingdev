<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'AskedBy', 'AskedOn', 'Body','Type','IsDeleted',
    ];

    protected $table ='questions';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function questionable()
    {
        return $this->morphTo();
    }

    public function comments()
    {
        return $this->morphMany('App\Models\Comment', 'commentable');
    }


    public function reviews()
    {
        return $this->morphMany('App\Models\Review', 'reviewable');
    }
}
