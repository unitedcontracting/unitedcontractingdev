<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
    protected $fillable = [
        'Interest', 'IsDeleted',
    ];

    protected $table ='interests';

    public function usersInterest()
    {
        return $this->belongsToMany('App\User');
    }

}
