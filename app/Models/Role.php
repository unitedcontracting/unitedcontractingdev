<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Name','IsDeleted',
    ];
    protected $table ='roles';

    public function users()
    {
        return $this->hasMany('App\User');
    }
}
