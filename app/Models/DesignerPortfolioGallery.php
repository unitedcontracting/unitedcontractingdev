<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DesignerPortfolioGallery extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ImageUrl', 'IsDeleted',
    ];

    protected $table ='designers_portfolio_gallery';

    public function portfolio()
    {
        return $this->belongsTo('App\Models\DesignerPortfolio');
    }
}
