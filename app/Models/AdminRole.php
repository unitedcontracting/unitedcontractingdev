<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminRole extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Name','IsDeleted',
    ];

    protected $table ='admins_roles';

    public function admins()
    {
        return $this->hasMany('App\Models\Admin');
    }
}
