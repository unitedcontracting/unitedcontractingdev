<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Name', 'Discount', 'FromDate','ToDate',
    ];

    protected $table ='promo_codes';

    public function cart()
    {
        return $this->hasMany('App\Models\Cart');
    }

    public function tempCart()
    {
        return $this->hasMany('App\Models\TempCart');
    }
}
