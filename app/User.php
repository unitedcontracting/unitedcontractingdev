<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'UserName', 'FirstName', 'LastName', 'Email', 'PhoneNumber', 'Password', 'IDRole',
        'City', 'Gender', 'Country', 'BirthDate', 'IsDeleted', 'WebSite', 'BlockedBy', 'Paned',
        'AboutMe', 'MyFavoriteStyle', 'MyNextHouseProject', 'Token', 'TokenType', 'email_verified_at'
    ];

    protected $primaryKey = 'IDUser';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'Password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getEmailAttribute()
    {
        return $this->attributes['Email'];
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function productsFavorite()
    {
        return $this->belongsToMany('App\Models\Product');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function cart()
    {
        return $this->belongsTo('App\Models\Cart');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function designerPortfolio()
    {
        return $this->hasMany('App\Models\DesignerPortfolio');
    }

    public function commentBy()
    {
        return $this->hasMany('App\Models\Comment');
    }

    public function questions()
    {
        return $this->hasMany('App\Models\Question');
    }

    public function complainsBy()
    {
        return $this->hasMany('App\Models\Complain');
    }

    public function complainsOn()
    {
        return $this->hasMany('App\Models\Complain');
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\Review');
    }

    public function ads()
    {
        return $this->hasMany('App\Models\Ad');
    }

    public function commentsOn()
    {
        return $this->morphMany('App\Models\Comment', 'commentable');
    }

    public function questionOn()
    {
        return $this->morphMany('App\Models\Question', 'questionable');
    }

    public function reviewOn()
    {
        return $this->morphMany('App\Models\Review', 'reviewable', 'Type', 'ReviewedOn');
    }

    public function subscribe()
    {
        return $this->belongsTo('App\Models\Subscription');
    }

    public function productsInterest()
    {
        return $this->belongsToMany('App\Models\Interest');
    }
}
