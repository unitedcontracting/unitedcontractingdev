<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-3 col-md-3">
                <h1>
                    <a class="logo" href="{{ url('/') }}">
                        <img src="{{asset('images/logo.jpg')}}" style="max-width: 150px">
                    </a>
                </h1>
                <button class="btn btn-primary">Button</button>
                <p class="mt-2">@2019 United Inc.</p>
            </div>
            <div class="col-12 col-sm-3 col-md-2">
                <h4 class="footer-title">Company</h4>
                <ul class="list-unstyled footer-list">
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">About Us</a></li>
                </ul>
            </div>
            <div class="col-12 col-sm-3 col-md-2">
                <h4 class="footer-title">Pro Services</h4>
                <ul class="list-unstyled footer-list">
                    <li><a href="#">advertisement</a></li>
                    <li><a href="#">sell</a></li>
                    <li><a href="#">trade program</a></li>
                    <li><a href="#">support</a></li>
                </ul>
            </div>
            <div class="col-12 col-sm-3 col-md-2">
                <h4 class="footer-title">Help Get</h4>
                <ul class="list-unstyled footer-list">
                    <li><a href="#">Support</a></li>
                    <li><a href="#">Support</a></li>
                    <li><a href="#">Support</a></li>
                    <li><a href="#">Support</a></li>

                </ul>
            </div>
            <div class="col-12 col-sm-3 col-md-3">
                <h4 class="footer-title">connect with us</h4>
                <ul class="list-unstyled footer-list">
                    <li><a href="#"><i class="la la-facebook"></i> Facebook</a></li>
                    <li><a href="#"><i class="la la-instagram"></i> instagram</a></li>
                    <li><a href="#"><i class="la la-twitter"></i> Twitter</a></li>
                    <li><a href="#"><i class="la la-linkedin"></i> LinkedIn</a></li>
                </ul>
            </div>
            <div class="col-12">
                <p class="copy-right">Need Help with an existing order ? call <strong>19866</strong> Sun - Thus</p>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.fancybox.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/script.js')}}"></script>
<script type="text/javascript" src="{{asset('js/myJs.js')}}"></script>