<div class="topbar">
    <div class="container">
        <div class="row align-content-center">
            <div class="col-12  col-md-6 col-lg-3">
                <a class="logo" href="{{ url('/') }}">
                    <img src="{{asset('images/logo.jpg')}}" style="max-width: 150px">
                </a>
            </div>
            <div class="col-12  col-md-6 col-lg-3 order-12">
                <ul class="list-unstyled list-inline topbar-list">

                    @if(Auth::guest())
                        <li class="notification">
                            <a href="#">
                                <i class="la la-shopping-cart"></i>
                                <span class="badge badge-light">{{@$GetTempCartVar}}</span>
                            </a>
                        </li>
                    @endif
                    @if(!Auth::guest())
                        <li class="notification">
                            <a href="#">
                                <i class="la la-shopping-cart"></i>
                                <span class="badge badge-light">{{@$GetCartVar}}</span>
                            </a>
                        </li>
                        <li class="notification"><a href="#"><i class="la la-heart-o"></i>
                                <span class="badge badge-light">{{@$GetFavoriteProductsVar}}</span></a>
                        </li>
                    @endif
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">
                                <i class="fas fa-user" aria-hidden="true"></i>
                                <span>{{ __('Login') }} </span>
                            </a>
                        </li>

                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">
                                    <i class="fas fa-user" aria-hidden="true"></i>
                                    <span>{{ __('Register') }} </span>
                                </a>
                            </li>
                        @endif
                    @else
                    <li>
                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <a> <i class="la la-user"></i>  {{ Auth::user()->userName() }}</a>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit Profile and Settings</a>
                                <a class="dropdown-item" href="#">your orders</a>
                                <div class="dropdown-divider"></div>

                                <a class="dropdown-item" href="#"><i class="la la-pencil mr-1"></i>Create new sketch</a>
                                <div class="dropdown-divider"></div>


                                <div class="dropdown-item" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </div>
                    </li>
                    @endguest
                </ul>
            </div>
            <div class="col-12  col-md-12 col-lg-6 order-1">
                <form class="search-form" action="{{url('search')}}" method="GET" role="search">
                    <input class="form-control" name="search" type="search" placeholder="Search Products , People and More"
                           aria-label="Search">
                    <i class="la la-search search-button"></i>
                </form>
            </div>
        </div>
    </div>
</div>

<nav class="navbar navbar-expand-lg navbar-light bg-light main-navbar">
    <a class="navbar-brand" href="#"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="{{url('/about')}}">
                    <i class="la la-users"></i>
                    <span>About</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/shop')}}">
                    <i class="la la-shopping-cart"></i>
                    <span>shop by product</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/designer')}}">
                    <i class="la la-user"></i>
                    <span>find professionals</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <i class="la la-pencil"></i>
                    <span>Draw your Dream</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <i class="la la-money"></i>
                    <span>sale</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <i class="la la-phone"></i>
                    <span>contact us</span>
                </a>
            </li>
            {{--<li class="nav-item dropdown">--}}
            {{--<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">--}}
            {{--<i class="la la-phone"></i>  Dropdown</a>--}}
            {{--<div class="dropdown-menu">--}}
            {{--<a class="dropdown-item" href="#">Action</a>--}}
            {{--<a class="dropdown-item" href="#">Another action</a>--}}
            {{--<a class="dropdown-item" href="#">Something else here</a>--}}
            {{--<div class="dropdown-divider"></div>--}}
            {{--<a class="dropdown-item" href="#">Separated link</a>--}}
            {{--</div>--}}
            {{--</li>--}}
        </ul>
    </div>
</nav>



