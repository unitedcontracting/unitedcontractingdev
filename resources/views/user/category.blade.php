@extends('layouts.app')

@section('content')
    @include("files/rotator-banner-with-gallery")
    @include("files/item-best-seller")
    @include("files/new-arrivals-carousel")
    @include("files/featured-review-carousel")
@endsection


