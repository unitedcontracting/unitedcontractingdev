@extends('layouts.app')

@section('content')
    @include("files/rotator-banner-with-menu")
    @include("files/shop-gallery")
    @include("files/item-on-sale-carousel")
    @include("files/new-arrivals-carousel")
@endsection
