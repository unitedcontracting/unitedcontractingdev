<div class="filtering">
    <div class="container">
        <div>
            <form>
                <div class="form-row align-items-center justify-content-center mt-3 select-theme">
                    <button class="btn btn-primary">show all</button>

                    <div class="col-auto my-1">
                        <label class="mr-sm-2 sr-only" for="inlineFormCustomSelect"></label>
                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                            <option selected>style</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                    <div class="col-auto my-1">
                        <label class="mr-sm-2 sr-only" for="inlineFormCustomSelect"></label>
                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                            <option selected>brand</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                    <div class="col-auto my-1">
                        <label class="mr-sm-2 sr-only" for="inlineFormCustomSelect"></label>
                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                            <option selected>color</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                    <div class="col-auto my-1">
                        <label class="mr-sm-2 sr-only" for="inlineFormCustomSelect"></label>
                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                            <option selected>shape</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                </div>
                <div class="form-row align-items-center justify-content-center mt-3">
                    <div class="col-auto my-1">
                        <label class="mr-sm-2 sr-only" for="inlineFormCustomSelect">Preference</label>
                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                            <option selected>price</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                    <div class="col-auto my-1">
                        <label class="mr-sm-2 sr-only" for="inlineFormCustomSelect">Preference</label>
                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                            <option selected>Customer Rating</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                    <div class="col-auto my-1">
                        <label class="mr-sm-2 sr-only" for="inlineFormCustomSelect">Preference</label>
                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                            <option selected>Sale</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                    <div class="col-auto my-1">
                        <label class="mr-sm-2 sr-only" for="inlineFormCustomSelect">Preference</label>
                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                            <option selected>10 per page</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                    <div>1-10 of 1000 products</div>
                </div>
            </form>

        </div>
        <div class="row">

        <div class="element-item col-12 col-sm-6 col-md-3 col-lg-3">
            <img src="{{asset('images/product3.jpg')}}" alt="" class="product-img">
        </div>


        <div class="element-item col-12 col-sm-6 col-md-3 col-lg-3">
            <img src="{{asset('images/product2.jpg')}}" alt="" class="product-img">
        </div>


        <div class="element-item col-12 col-sm-6 col-md-3 col-lg-3" >
            <img src="{{asset('images/product1.jpg')}}" alt="" class="product-img">
        </div>


        <div class="element-item col-12 col-sm-6 col-md-3 col-lg-3" >
            <img src="{{asset('images/product4.jpg')}}" alt="" class="product-img">
        </div>


        <div class="element-item col-12 col-sm-6 col-md-3 col-lg-3">
            <img src="i{{asset('images/product6.jpg')}}" alt="" class="product-img">
        </div>



    </div>
    <div class="main-pagination">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">Next</a></li>
            </ul>
        </nav>
    </div>

</div>
</div>
