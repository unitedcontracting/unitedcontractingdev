<div class="home-imporovment">
    <div class="container">
        <div class="row">
            @foreach($bestUserRating as $user)
                <div class="col-12">
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img src="{{asset($user['ImagePortfolio'])}}" alt="" class="item-img">
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="media mb-3">
                                    <img src="{{asset($user['Image'])}}" class="align-self-start mr-3 item-small-img"
                                         alt="...">
                                    <div class="media-body">
                                        <h5 class="mt-0">{{$user['Name']}}</h5>
                                        <div>
                                            <div class="star-ratings">
                                                <div class="fill-ratings" style="width:{{$user['RatingPercent']}}%;">
                                                    <span>★★★★★</span>
                                                </div>
                                                <div class="empty-ratings">
                                                    <span>★★★★★</span>
                                                </div>
                                            </div>
                                            <span>{{$user['Reviewer']}} Reviews</span>
                                        </div>
                                    </div>
                                </div>
                                <p>{{$user['About']}}</p>
                                <a href="#" class="btn btn-primary">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="col-12">
                <div class="main-pagination">
                    <nav aria-label="Page navigation example">
                        {{ $bestUserRating->links() }}
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
