<div class="categories">
    <div class="container">
        <h1 class="main-title">Items on Sale</h1>
        <div class="owl-carousel">
            @foreach($GetProductOnSaleVar as $onSale)
                <div class="item"><a href="#"><img src="{{asset($onSale['ImagesAdsUrl'])}}"></a></div>
            @endforeach
        </div>
    </div>
</div>
