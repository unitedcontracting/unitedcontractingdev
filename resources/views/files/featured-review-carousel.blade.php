<div class="categories">
    <div class="container">
        <h1 class="main-title">featured review</h1>
        <div class="owl-carousel">
            @foreach($GetProductsReviewsVar as $product)
            <div class="item"><a href="#"><img src="{{asset($product['image'])}}"></a></div>
            @endforeach
        </div>
    </div>
</div>
