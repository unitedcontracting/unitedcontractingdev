<div class="gallery">
    <div class="container">
        <div class="row">
            @foreach($GetCategoryWithSubCategoryVar as $sub)
                <div class="col-12 col-sm-4">
                    <a class="gallery-item" href="#" data-fancybox="images"
                       data-caption="Backpackers following a dirt trail">
                        <img src="{{asset($sub['ImageUrl'])}}" class="img-fluid"/>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>
