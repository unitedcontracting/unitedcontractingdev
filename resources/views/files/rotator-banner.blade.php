<div id="carouselExampleSlidesOnly" class="carousel slide rotator-banner" data-ride="carousel">
    <div class="carousel-inner">
        @foreach($GetAdsVar as $Ad)
        <div class="carousel-item {{$Ad['Active']}}">
            <div class="carousel-caption">
                <h1>Rotator Banner</h1>
                <button class="btn btn-primary">Button</button>
            </div>
            <img src="{{asset($Ad['ImageUrl'])}}" class="d-block w-100" alt="...">
        </div>
        @endforeach
    </div>
</div>
