<div class="categories">
    <div class="container">
        <h1 class="main-title">Find Professionals</h1>
        <div class="owl-carousel">
            @foreach($GetDesignersVar as $designer)
                <div class="item"><a href="#"><img src="{{asset($designer['Image'])}}"></a></div>
            @endforeach
        </div>
    </div>
</div>
