<div class="categories">
    <div class="container">
        <h1 class="main-title">new arrivals</h1>
        <div class="owl-carousel">
            @foreach($GetProductNewArrivalVar as $new)
            <div class="item"><a href="#"><img src="{{asset($new['ImagesAdsUrl'])}}"></a></div>
            @endforeach
        </div>
    </div>
</div>
