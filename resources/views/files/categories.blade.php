<div class="categories">
    <div class="container">
        <h1 class="main-title">Categories</h1>
        <div class="owl-carousel">
            @foreach($GetCategoriesVar as $category)
            <div class="item"><a href="#"><img src={{asset($category['ImageUrl'])}}></a></div>
            @endforeach
        </div>
    </div>
</div>
