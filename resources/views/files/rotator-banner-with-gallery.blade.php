<div class="rotator-banner-with-menu rotator-banner-with-gallery">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-3">
                <ul class="list-group menu">
                    <li class="list-group-item active"><a href="#">categories left menu</a></li>
                    @foreach($GetCategoriesVar as $category)
                    <li class="list-group-item"><a href="{{url('category/'. $category['IDCategory'])}}">{{$category['Name']}}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-12 col-sm-9">
                <div class="mb-5">
                    <h3 class="text-primary">categories headline</h3>
                    <p>categories brief categories brief categories brief categories brief categories brief categories briefcategories brief categories brief categories brief categories brief</p>
                </div>
                <div class="row">
                    @foreach($GetCategoryWithSubCategoryVar as $sub)
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <a class="gallery-item" href="{{url('sub_category/'. $sub['IDCategory'])}}"  data-caption="Backpackers following a dirt trail">
                            <img src="{{asset($sub['ImageUrl'])}}" class="img-fluid"/>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
