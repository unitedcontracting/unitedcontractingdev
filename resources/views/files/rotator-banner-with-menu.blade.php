<div class="rotator-banner-with-menu">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-3">
                <ul class="list-group menu">
                    <li class="list-group-item active"><a href="#">categories left menu</a></li>
                    @foreach($GetCategoriesVar as $category)
                        <li class="list-group-item"><a href="{{url('category/'. $category['IDCategory'])}}">{{$category['Name']}}</a></li>
                    @endforeach
                </ul>
            </div>

            <div class="col-12 col-sm-9">
                <div id="carouselExampleSlidesOnly" class="carousel slide rotator-banner" data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach($GetAdsVar as $Ad)
                            <div class="carousel-item {{$Ad['Active']}}">
                                <div class="carousel-caption">
                                    <h1>Rotator Banner</h1>
                                    <button class="btn btn-primary">Button</button>
                                </div>
                                <img src="{{asset($Ad['ImageUrl'])}}" class="d-block w-100" alt="...">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
