<div class="gallery">
    <div class="container">
        <h1 class="main-title">Gallery</h1>

        <div class="row">
            @foreach($GetProductsVar as $product)
            <div class="col-12 col-sm-4">
                <a class="gallery-item" href="#" data-fancybox="images" data-caption="Backpackers following a dirt trail">
                    <img src="{{asset($product['ImagesAdsUrl'])}}" class="img-fluid"/>
                </a>
            </div>
            @endforeach
        </div>

        <div class="col-12">
            <div class="main-pagination">
                <nav aria-label="Page navigation example">
                    {{ $GetProductsVar->links() }}
                </nav>
            </div>
        </div>
    </div>

    </div>
</div>
