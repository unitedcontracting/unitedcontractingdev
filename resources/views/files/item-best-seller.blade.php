<div class="categories">
    <div class="container">
        <h1 class="main-title"> Shop Best Sellers</h1>
        <div class="owl-carousel">
            @foreach($GetProductsBestSellerVar as $best)
                <div class="item"><a href="#"><img src="{{asset($best['ImagesAdsUrl'])}}"></a></div>
            @endforeach

        </div>
    </div>
</div>
