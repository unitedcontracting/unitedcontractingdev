<div class="categories">
    <div class="container">
        <h1 class="main-title">Designers Categories</h1>
        <div class="owl-carousel">
            <div class="item"><a href="#"><img src="images/product1.jpg"></a></div>
            <div class="item"><a href="#"><img src="images/product2.jpg"></a></div>
            <div class="item"><a href="#"><img src="images/product3.jpg"></a></div>
            <div class="item"><a href="#"><img src="images/product4.jpg"></a></div>
            <div class="item"><a href="#"><img src="images/product5.jpg"></a></div>
            <div class="item"><a href="#"><img src="images/product6.jpg"></a></div>
            <div class="item"><a href="#"><img src="images/product9.jpg"></a></div>
            <div class="item"><a href="#"><img src="images/product8.jpg"></a></div>
        </div>
    </div>
</div>