<div class="draw-your-dream-section">
    <div class="container">
        <h1 class="main-title text-center">Draw Your Dream</h1>
        <div class="row">
            <div class="col-12 col-sm-6 col-md-4">
                <div class="draw-your-dream-section__item">
                    <i class="la la-umbrella"></i>
                    <p>discover design idea</p>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <div class="draw-your-dream-section__item">
                    <i class="la la-user"></i>
                    <p>find home professionals</p>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <div class="draw-your-dream-section__item">
                    <i class="la la-heart-o"></i>
                    <p>shop for home</p>
                </div>
            </div>
        </div>
        <button class="btn btn-primary mt-5">Button</button>
    </div>
</div>
