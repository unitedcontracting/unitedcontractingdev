@extends('layouts.app')

@section('content')
    @include("files.rotator-banner")
    @include("files.categories")
    @include("files.find-professionals")
    @include("files.draw-your-dream-section")
    @include("files.gallery")
@endsection

