$(document).ready(function () {
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            navText: [
                "<i class='la la-angle-left'></i>",
                "<i class='la la-angle-right'></i>"
            ],
            autoplay: true,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1,
                    nav: false,
                    dots:true
                },
                600: {
                    items: 3,
                    nav: false,
                    dots:true
                },
                1000: {
                    items: 4,
                    dots:false
                }
            }
        });


});


